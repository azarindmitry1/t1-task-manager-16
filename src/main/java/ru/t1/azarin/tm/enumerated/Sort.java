package ru.t1.azarin.tm.enumerated;

import ru.t1.azarin.tm.comparator.CreatedComparator;
import ru.t1.azarin.tm.comparator.NameComparator;
import ru.t1.azarin.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE);

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public Comparator getComparator() {
        return comparator;
    }

}