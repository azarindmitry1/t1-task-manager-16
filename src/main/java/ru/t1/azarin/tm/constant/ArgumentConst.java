package ru.t1.azarin.tm.constant;

public final class ArgumentConst {

    public static final String ARG_VERSION = "-v";

    public static final String ARG_ABOUT = "-a";

    public static final String ARG_HELP = "-h";

    public static final String ARG_INFO = "-i";

    public static final String ARG_ARGUMENTS = "-arg";

    public static final String ARG_COMMANDS = "-cmd";

}