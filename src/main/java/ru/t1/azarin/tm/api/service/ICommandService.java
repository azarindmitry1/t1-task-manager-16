package ru.t1.azarin.tm.api.service;

import ru.t1.azarin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}