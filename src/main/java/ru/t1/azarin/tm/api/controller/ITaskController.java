package ru.t1.azarin.tm.api.controller;

import ru.t1.azarin.tm.model.Task;

import java.util.List;

public interface ITaskController {

    void createTask();

    void clearTasks();

    List<Task> renderTask(List<Task> tasks);

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void showTaskByProjectId();

}