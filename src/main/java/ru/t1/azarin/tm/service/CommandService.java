package ru.t1.azarin.tm.service;

import ru.t1.azarin.tm.api.repository.ICommandRepository;
import ru.t1.azarin.tm.api.service.ICommandService;
import ru.t1.azarin.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}